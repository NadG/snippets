// #region Setup
const API_URL = "https://starwars.egghead.training/";
const output = document.getElementById('output');
const spinner = document.getElementById('spinner');

//const responsePromise = fetch(API_URL + 'films');
function queryAPI(endpoint) {
    return fetch(API_URL + endpoint).then(res => {
        return res.ok ? res.json() : Promise.reject(Error('Unsuccessful response'))
    })
}
// //////// endregion///////////////////////////////////////////////////
// ASYNC//////////////////////////////////////////////////////////////
async function main() {
    try {
        const [films, planets, species] = await Promise.all([
            queryAPI('films'),
            queryAPI('planets'),
            queryAPI('species')
        ]);
        output.innerText =
            `${films.length} films, ` +
            `${planets.length} planets` +
            `${species.length} species` +
    } catch (error) {
        console.log(error);
    } finally {
        spinner.remove()
    }
}
main();
// const [films, planets, species] => destructuring
// Promise.all permette di eseguire in parallelo tutte le chiamate, indipendenti, evitando
//che le promise successive vengano eseguite quando la precedente si completa
// (come se le query dipendessero tra loro
// try { }  permette l'uso del catch() qualora ci sia una rejection, evitando di dover fare
// anti-estetico main().catch(), e avere tutta la logica in un posto solo non duplicata
// /////////////////////////////////////////////////////////////////////

// PROMISE.ALL//////////////////////////////////////////////////////
const promise = Promise.all([
    queryAPI("films"),
    queryAPI("planets")
]);

promise
    .then(([films, planets]) => {
        output.innerText =
            `${films.length} films, ` +
            `${planets.length} planets`
    })
    .catch( err => console.log(err))
    .finally(() => spinner.remove());
/////////////////////////////////////////////////////////////////////////
